#include "lista.h"

typedef struct lista
{
    nodo_t* primero;
    nodo_t* ultimo;
    size_t tamanio;
} t_lista;

typedef struct iterador
{
    nodo_t* anterior;
    nodo_t* actual;
} t_lista_iter;

nodo_t* lista_get_primero(lista_t* list)
{
    return list->primero;
}

void lista_set_primero(lista_t* list,nodo_t* prim)
{
    list->primero=prim;
}

nodo_t* lista_get_ultimo(lista_t* list)
{
    return list->ultimo;
}

size_t lista_get_tamanio(lista_t* list)
{
    return list->tamanio;
}

void lista_set_ultimo(lista_t* list,nodo_t* ult)
{
    list->ultimo=ult;
}

lista_t* lista_crear()
{
    lista_t* lista=malloc(sizeof(lista_t));
    if (lista == NULL) return NULL;
    lista->primero=NULL;
    lista->ultimo=NULL;
    lista->tamanio=-1;
    return lista;
}

bool lista_esta_vacia(const lista_t *lista)
{
    if (lista->primero==NULL)
    {
        return true;
    }
    return false;
}

bool lista_insertar_primero(lista_t *lista, void *dato)
{
    nodo_t *nodo_aux;
    nodo_aux=nodo_crear(dato);
    if (nodo_aux==NULL) return false;
    lista->tamanio++;
    nodo_set_siguiente(nodo_aux,lista->primero);
    lista->primero=nodo_aux;
    if (lista->ultimo==NULL)
        lista->ultimo=nodo_aux;
    return true;

}

bool lista_insertar_ultimo(lista_t *lista, void *dato)
{
    nodo_t *nodo_aux;
    nodo_aux=nodo_crear(dato);
    if (nodo_aux==NULL) return false;
    nodo_set_siguiente(nodo_aux,NULL);
    lista->tamanio++;
    if (lista->ultimo==NULL)
    {
        lista->primero=nodo_aux;
        lista->ultimo=nodo_aux;
    }
    else
    {
        nodo_set_siguiente(lista->ultimo,nodo_aux);
        lista->ultimo=nodo_aux;
    }
    return true;

}
void *lista_borrar_primero(lista_t *lista)
{
    nodo_t* nodo_aux;
    void *dato_aux;
    if (lista_esta_vacia(lista))return NULL;
    dato_aux=nodo_get_dato(lista->primero);
    lista->tamanio-=1;
    nodo_aux=lista->primero;
    if (lista->primero==lista->ultimo)
    {
        lista->primero=NULL;
        lista->ultimo=NULL;
    }
    else
    {

        lista->primero=nodo_get_siguiente(lista->primero);
    }
    free(nodo_aux);
    return dato_aux;
}

void *lista_ver_primero(const lista_t *lista)
{
    void *dato;
    if(lista_esta_vacia(lista))
    {
        return NULL;
    }
    dato=nodo_get_dato(lista->primero);
    return dato;
}

size_t lista_largo(const lista_t *lista)
{
    return (lista->tamanio+1);
}

void lista_destruir(lista_t *lista, void destruir_dato(void *))
{
    nodo_t*nodo_aux;
    nodo_t*temp;
    nodo_aux=lista->primero;
    if(!lista_esta_vacia(lista))
    {
        while (nodo_aux)
        {
            temp=nodo_aux;
            nodo_aux=nodo_get_siguiente(nodo_aux);
            nodo_destruir(temp,destruir_dato);
            free(temp);
        }
    }
    free(lista);
}

lista_iter_t *lista_iter_crear(lista_t *lista)
{
    lista_iter_t* aux;
    aux=malloc(sizeof(lista_iter_t));
    aux->actual=lista->primero;
    aux->anterior=NULL;
    return aux;
}

bool lista_iter_avanzar(lista_iter_t *iter)
{
    nodo_t* aux;
    aux=lista_ver_siguiente(iter);
    if (iter->actual==NULL)
    {
        return false;
    }
    else
    {
        iter->anterior=iter->actual;
        iter->actual=aux;
        return true;
    }
}

void *lista_iter_ver_actual(lista_iter_t *iter)
{
    if (iter->actual!=NULL)
    {
        return nodo_get_dato(iter->actual);
    }
    else
    {
        return NULL;
    }
}

bool lista_iter_al_final(const lista_iter_t *iter)
{
    if (iter->actual==NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void lista_iter_destruir(lista_iter_t *iter)
{
    free(iter);
}

bool lista_insertar(lista_t *lista, lista_iter_t *iter, void *dato)
{
    nodo_t *nodo_aux;
    nodo_aux=nodo_crear(dato);
    if (nodo_aux==NULL) return false;
    if (lista_esta_vacia(lista))
    {
        nodo_set_siguiente(nodo_aux,NULL);
        lista->primero=nodo_aux;
        lista->ultimo=nodo_aux;
        lista->tamanio++;
        iter->actual=lista->primero;
        return true;
    }
    else
    {
        if (iter->actual==NULL)
        {
            iter->actual=nodo_aux;
            nodo_set_siguiente(iter->anterior,iter->actual);
            nodo_set_siguiente(iter->actual,NULL);
        }
        else
        {
            if(iter->anterior==NULL)
            {
                iter->anterior=nodo_aux;
                nodo_set_siguiente(iter->anterior,iter->actual);
                lista->primero=iter->anterior;
                lista->ultimo=iter->actual;
                iter->actual=iter->anterior;
                iter->anterior=NULL;
                lista->tamanio++;
                return true;
            }
            nodo_set_siguiente(nodo_aux,iter->actual);
            nodo_set_siguiente(iter->anterior,nodo_aux);
            iter->actual=nodo_aux;
        }
    }
    lista->tamanio++;
    return true;
}

void *lista_borrar(lista_t *lista, lista_iter_t *iter,void destruir_dato(void *))
{
    void* retorno;
    nodo_t* nodo_borrar;
    if (lista_esta_vacia(lista))
    {
        retorno=NULL;
    }
    else
    {
        nodo_borrar=iter->actual;
        if (iter->actual!=NULL)
        {
            retorno=((diccionario_t*)nodo_get_dato(iter->actual))->dato;
            if (iter->anterior==NULL)
            {
                if (nodo_get_siguiente(iter->actual)==NULL)
                {
                    lista->primero=NULL;
                    lista->ultimo=NULL;
                    iter->actual=lista->primero;
                }
                else
                {
                    lista->primero=nodo_get_siguiente(iter->actual);
                    iter->actual=nodo_get_siguiente(iter->actual);
                }
            }
            else
            {
                nodo_set_siguiente(iter->anterior,nodo_get_siguiente(iter->actual));
                if (iter->actual==lista->ultimo)
                {
                    lista->ultimo=iter->anterior;
                }
                iter->actual=nodo_get_siguiente(iter->actual);
            }
            lista->tamanio--;
            /*if(destruir_dato==NULL)
            {
                destruir_dato(((diccionario_t*)nodo_get_dato(nodo_borrar))->dato);
                free((diccionario_t*) nodo_get_dato(nodo_borrar));
            }else{*/
                nodo_destruir(nodo_borrar,destruir_dato);
            /*}*/
            free(nodo_borrar);
        }
        else
        {
            retorno=NULL;
        }
    }
    return retorno;
}

void* lista_ver_siguiente(lista_iter_t *iter)
{
    if (iter->actual!=NULL)
    {
        return nodo_get_siguiente(iter->actual);
    }
    else
    {
        return NULL;
    }
}

