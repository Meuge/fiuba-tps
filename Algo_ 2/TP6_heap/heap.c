#include "heap.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define CANT 15

struct heap_t
{
    void** datos;
    size_t cant, tam;
    cmp_func_t cmp;
};

bool heap_redimensionar(heap_t*heap,bool agrandar)
{
    void **datos_nuevo;
    if (agrandar)
    {
        heap->tam=heap->tam + CANT;
    }
    else
    {
        heap->tam=heap->tam - CANT;
    }
    datos_nuevo= realloc(heap->datos,(heap->tam*sizeof(void*)));
    if (datos_nuevo == NULL)
    {
        free(heap->datos);
        return false;
    }
    heap->datos = datos_nuevo;
    return true;
}


bool upheap(const heap_t*heap){

    void* ultimo_ingresado=heap->datos[heap->cant];
    int cant=heap->cant;
    while (cant>=1 && heap->cmp(heap->datos[((cant-1)/2)],ultimo_ingresado)==-1) // Cant tiene que ser mayor a 1 asi no queda fuera de index
    {
        heap->datos[cant]=heap->datos[(cant-1)/2];
        cant=(cant-1)/2;
    }
    heap->datos[cant]=ultimo_ingresado;
    return true;

}
heap_t *heap_crear(cmp_func_t cmp)
{
    heap_t* heap=malloc(sizeof(heap_t));
    if(heap==NULL)return NULL;
   
    heap->datos=calloc(CANT,sizeof(void*));
    if (heap->datos==NULL)
    {
        free(heap);
        return NULL;
    }
    heap->tam=CANT;
    heap->cant=-1;
    heap->cmp=cmp;
    return heap;
}

size_t heap_cantidad(const heap_t *heap)
{
    if(heap==NULL)return 0;
    return heap->cant + 1;
}

bool heap_esta_vacio(const heap_t *heap)
{
    if(heap->cant==-1)return true;
    return false;
}

bool heap_encolar(heap_t *heap, void *elem)
{
    if (heap->cant==heap->tam-1)
    {
       heap_redimensionar(heap,true);
    }
    heap->cant ++ ;
    heap->datos[heap->cant]=elem;
    upheap(heap);
  
   
    return true;
}

void *heap_ver_max(const heap_t *heap)
{
    if(heap_esta_vacio(heap))return NULL;
    return heap->datos[0];
}

void downheap(int posicion,void *vector[],size_t largo,cmp_func_t cmp) 
{
    int hijo = (2*posicion)+1; //HIjo izquierdo
    void *temp;                

    while (hijo<=largo) 
    {
        if (hijo<largo && cmp(vector[hijo],vector[hijo+1])==-1) //Si hijo es menor al largo del arreglo, significa que hay otro hijo
        {
            hijo++; //Se aumenta en caso de que el hijo derecho sea mayor al hijo izquierdo
        }

        if (cmp(vector[posicion],vector[hijo])==-1)
        {
            temp = vector[posicion];   
            vector[posicion] = vector[hijo];
            vector[hijo] = temp;
            posicion = hijo;           //Hago intercambio
            hijo = (2*posicion)+1;     //continuo ordenando hijo
        }
        else
        {
            return;
        }
    }
}


void *heap_desencolar(heap_t *heap)
{   
    if (heap_esta_vacio(heap))return NULL;
    void* dato=heap->datos[0];
    if (heap->cant==0){ // Caso borde en el que hay un solo elemento en el heap
        heap->datos[heap->cant]=NULL;
        heap->cant-=1;
        return dato;
    }
    heap->datos[0]=heap->datos[heap->cant];
    heap->datos[heap->cant]=NULL;
    heap->cant=heap->cant-1;
    if (heap->cant+16==heap->tam){ 
      heap_redimensionar(heap,false);
    }
    if (heap->cant>=1){ //No tiene sentido llamar a downheap en caso de que no haya mas de un elemento
            downheap(0,heap->datos,heap->cant,heap->cmp);}

    return dato;
}

bool heap_destruir(heap_t *heap, void destruir_elemento(void *e))
{
    if(heap==NULL)return false; //No fue creado el Heap
    
    if(destruir_elemento)
    {
            int i;
            for(i=0; i<=heap->cant; i++)
            {
                destruir_elemento(heap->datos[i]);
            }
        
    }
    if(heap->datos){
        free(heap->datos);
        free(heap);
    }
    return true;
}
 

void heapsort( void *vector[],size_t largo, cmp_func_t cmp){

    int i;
    void *temp;

    for (i=((largo-1)/2); i>=0; i--) 
    {
        downheap(i,vector,largo-1,cmp); //Creo el heap
    }
    
     for(i = largo-1; i > 0; i--){
        temp = vector[i]; 
        vector[i] = vector[0];   //Saco el elemento más grande
        vector[0] = temp;

        downheap(0,vector,i-1,cmp);}  //Aplico propiedad del heap


}

