#include "heap.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

/* ******************************************************************
 *                        PRUEBAS UNITARIAS
 * *****************************************************************/

/* Función auxiliar para imprimir si estuvo OK o no. */
void print_test(char* name, bool result, int valor)
{
    printf("%s: %s %d\n", name, result? "OK" : "ERROR", valor);
}

/* Prueba que las primitivas del heap funcionen correctamente. */

int comparame (const void* valor1, const void* valor2){
		if (*(int*)valor1>*(int*)valor2){
		return 1;}
		else {if(*(int*)valor1==*(int*)valor2){ return 0;} else
		{return -1;}}
}
int mayor(const void* valor1, const void* valor2){
		if (*(int*)valor1>*(int*)valor2){
		return -1;}
		else {if(*(int*)valor1==*(int*)valor2){ return 0;} else
		{return 1;}}
}


cmp_func_t cmp_puntero= &comparame;
void random_inicializar(){
	unsigned int seed = (unsigned int)time(NULL);
	srand (seed);
}

int nuestrorandom(int lim){
	return rand()%lim;
}

// Funciones de swapeo
void swap_int(int** x, int** y){
	int* aux=*x;
	*x=*y;
	*y=aux;
}

// funcion que desordena el vector
void vector_desordenar(int* valores[], size_t largo){
	random_inicializar();
	size_t i;
	// es importante que el par clave-valor se mantenga siempre igual
	for (i=0; i<largo;i++){
		int rnd=nuestrorandom(largo);
		swap_int(&valores[i], &valores[rnd]);
	}
}

void prueba_heap(){
int num1=111;
int num2=753;
int num3=834;
int num4=33;
int num5=6;
int num6=98;
int j=8888;
int num7=9999;
int num8=8;
int i=2;


heap_t* heap_1 = heap_crear(*comparame);

print_test("Prueba verificar que el heap este vacio", heap_esta_vacio(heap_1),1);
print_test("Prueba heap insertar num1=111", heap_encolar(heap_1,&num1),1);
print_test("Prueba heap insertar num2=753", heap_encolar(heap_1,&num2),1);
print_test("Prueba heap insertar num3=834", heap_encolar(heap_1,&num3),1);
print_test("Prueba cantidad del heap", heap_cantidad(heap_1)==3,1);
print_test("Prueba heap desencolar maximo", *(int*)heap_desencolar(heap_1)==num3,1);
print_test("Prueba heap desencolar maximo", *(int*)heap_desencolar(heap_1)==num2,1);
print_test("Prueba heap desencolar maximo", *(int*)heap_desencolar(heap_1)==num1,1);
print_test("Prueba heap desencolar maximo es NUll", heap_desencolar(heap_1)==NULL,1);
print_test("Prueba heap insertar num1=111", heap_encolar(heap_1,&num1),1);
print_test("Prueba heap insertar num2=753", heap_encolar(heap_1,&num2),1);
print_test("Prueba heap insertar num3=834", heap_encolar(heap_1,&num3),1); 
print_test("Prueba heap insertar num4=33", heap_encolar(heap_1,&num4),1);
print_test("Prueba heap insertar num5=6", heap_encolar(heap_1,&num5),1);
print_test("Prueba cantidad del heap", heap_cantidad(heap_1)==5,1);
print_test("Prueba heap insertar num6=98", heap_encolar(heap_1,&num6),1);
print_test("Prueba heap ver max", *(int*)heap_ver_max(heap_1)==num3,1);
print_test("Prueba heap desencolar maximo", *(int*)heap_desencolar(heap_1)==num3,1);
print_test("Prueba heap ver max", *(int*)heap_ver_max(heap_1)==num2,1);
print_test("Prueba heap desencolar maximo", *(int*)heap_desencolar(heap_1)==num2,1);
print_test("Prueba heap ver max", *(int*)heap_ver_max(heap_1)==num1,1);
print_test("Prueba heap desencolar maximo", *(int*)heap_desencolar(heap_1)==num1,1);
print_test("Prueba heap ver max", *(int*)heap_ver_max(heap_1)==num6,1);
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));
print_test("Prueba heap desencolar maximo", true,heap_desencolar(heap_1)==NULL);
i=2;
print_test("Prueba heap insertar num=2 ",1, heap_encolar(heap_1,&i));
print_test("Prueba heap insertar num=8 ",1, heap_encolar(heap_1,&num8));
print_test("Prueba heap ver max",1, *(int*)heap_ver_max(heap_1));
print_test("Prueba heap insertar num 9999 ",1, heap_encolar(heap_1,&num7));
print_test("Prueba heap ver max",1, *(int*)heap_ver_max(heap_1));
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));
print_test("Prueba heap insertar num=8888 ",1, heap_encolar(heap_1,&j));
print_test("Prueba heap ver max",1, *(int*)heap_ver_max(heap_1));
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));
print_test("Prueba heap desencolar maximo", true,*(int*)heap_desencolar(heap_1));; 
print_test("Prueba heap desencolar maximo",true,heap_desencolar(heap_1)==NULL); 
print_test("Prueba heap esta vacio",heap_esta_vacio(heap_1)==true,1); 
heap_destruir(heap_1,NULL);


}
void pruebas_volumen(){

	heap_t* heap=heap_crear(comparame);

	int i=0;
	size_t cantidad=300;
	bool encola_todos = true;
	int* valores[cantidad];

	for (i = 0; i < cantidad; i++) {
		valores[i] = malloc(sizeof(int));
		*valores[i] = i;
	}
	i=0;

	while (i<cantidad && encola_todos){


		encola_todos = heap_encolar(heap,valores[i]);

		i++;
	}
	print_test("Prueba encolar una gran cantidad de elementos",1, encola_todos);

	print_test("El heap tiene elementos",1, heap_cantidad(heap)==cantidad);
	print_test("El heap no esta vacio",1, !heap_esta_vacio(heap));

	i=cantidad-1;
	encola_todos = true;

	while (i>=0 && encola_todos){

		encola_todos = (int*)heap_desencolar(heap)==valores[i];
		i--;
	}

	print_test("El heap no tiene elementos", 1,heap_cantidad(heap));
	print_test("El heap esta vacio",1, heap_esta_vacio(heap));

	print_test("Puedo desencolar una gran cantidad de elementos",1, encola_todos);

	for (i = 0; i < cantidad; i++) {
		heap_encolar(heap,valores[i]);
	}

	print_test("Encolar elementos nuevamente",1, encola_todos);

	print_test("El heap tiene largo elementos",1, heap_cantidad(heap)==cantidad);
	print_test("El heap no esta vacio",1, !heap_esta_vacio(heap));

	heap_destruir(heap,free);
}

void prueba_ordenamiento(){
	 printf("Pruebas de heapsort \n");
	 int* valores[10];
	 int* elementos[10];
	 bool verdadero;
	 int i;
 
	 valores[0]=malloc(sizeof(int));
	 *valores[0]=5;
	 valores[1]=malloc(sizeof(int));
	 *valores[1]=1;
	 valores[2]=malloc(sizeof(int));
	 *valores[2]=7;
	 valores[3]=malloc(sizeof(int));
	 *valores[3]=4;
	 valores[4]=malloc(sizeof(int));
	 *valores[4]=9;
	 valores[5]=malloc(sizeof(int));
	 *valores[5]=0;
	 valores[6]=malloc(sizeof(int));
	 *valores[6]=8;
	 valores[7]=malloc(sizeof(int));
	 *valores[7]=2;
	 valores[8]=malloc(sizeof(int));
	 *valores[8]=3;
	 valores[9]=malloc(sizeof(int));
	 *valores[9]=6; 
	 elementos[0]=malloc(sizeof(int));
	 *elementos[0]=0;
	 elementos[1]=malloc(sizeof(int));
	 *elementos[1]=1;
	 elementos[2]=malloc(sizeof(int));
	 *elementos[2]=2;
	 elementos[3]=malloc(sizeof(int));
	 *elementos[3]=3;
	 elementos[4]=malloc(sizeof(int));
	 *elementos[4]=4;
	 elementos[5]=malloc(sizeof(int));
	 *elementos[5]=5;
	 elementos[6]=malloc(sizeof(int));
	 *elementos[6]=6;
	 elementos[7]=malloc(sizeof(int));
	 *elementos[7]=7;
	 elementos[8]=malloc(sizeof(int));
	 *elementos[8]=8;
	 elementos[9]=malloc(sizeof(int));
	 *elementos[9]=9;

	 
	 heapsort((void**)valores,10,comparame);
	 print_test("Realiza heapsort",1, true);

    bool ok;
    for (int i = 0; i < 10; i++) {
        ok = *(valores[i]) == i;
        if (!ok) break;
    }
    print_test("Prueba ordenar de menor a mayor",1, ok);
    verdadero=true;
	 i=0;
	while (i < 10 ){
		 verdadero = (*elementos[i]==*valores[i]);
		 i++;
	 }
	print_test("El vector tratado esta ordenado",1, verdadero);
	 

	heapsort((void**)valores, 10, mayor);
    for (int i = 10-1; i >= 0; i--) {
        ok = ( *(valores[(10-1)-i]) == i );
        if (!ok) break;
    }
    print_test("Prueba ordenar de mayor a menor",1, ok);
	
	 
	for (i = 0; i < 10; i++) {
		 free(valores[i]);
		free(elementos[i]);
	 }
 }

  void prueba_sort_volumen(){
	 printf("Pruebas de volumen de heap sort\n");
	 int i=0;
	 size_t largo=10;
	 bool todobien;
	 int* valores[largo];
	 int* orden[largo];
	
	for (i = 0; i < largo; i++) {
		valores[i] = malloc(sizeof(int));
		*valores[i] = i;
	}
	 for (i = 0; i < largo; i++) {
		orden[i] = malloc(sizeof(int));
		 *orden[i] = i;
	}
	
	 vector_desordenar(valores,largo);
	 
	todobien=true;
	i=0;
	 while (i < largo && todobien){
		 todobien = (*orden[i]==*valores[i]);
		 i++;
	}
	 
	 print_test("El vector a tratar esta desordenado",1, !todobien);

	 heapsort((void**)valores,largo,comparame);
	 print_test("Hago heap_sort",1, true);
	//

	//~ 
	 todobien=true;
	 i=0;
	 while (i < largo && todobien){
		todobien = (*orden[i]==*valores[i]);
		 i++;
	}
	print_test("El vector tratado esta ordenado",1, todobien);
	//~ 
	 for (i = 0; i < largo; i++) {
		free(valores[i]);
		 free(orden[i]);
	 }
//~ 
 }
 
void prueba_encolar2_desencolar_encolar(){
    char *uno="100", *dos="200", *tres="300";

    heap_t *heap = heap_crear((cmp_func_t) strcmp);

    print_test("Prueba encolar primer elemento",1, heap_encolar(heap, uno) );
    print_test("Prueba encolar segundo elemento",1, heap_encolar(heap, dos) );
    print_test("Prueba desencolar un elemento", 1,heap_desencolar(heap) == dos );
    print_test("Prueba heap la cantidad es 1",1, heap_cantidad(heap) == 1);

    print_test("Prueba volver a encolar",1, heap_encolar(heap, tres) );
    print_test("Prueba volver a desencolar",1, heap_desencolar(heap) == tres );
    print_test("Prueba heap no esta vacio",1, !heap_esta_vacio(heap));
    print_test("Prueba heap la cantidad es 1",1, heap_cantidad(heap) == 1);

    heap_destruir(heap, NULL);
}
/*
void prueba_heap_volumen(size_t largo, bool debug)
{
    heap_t *heap;
    int **valores = generar_arreglo(largo);*/
    /* Heap de maximo */
	 //heap = heap_crear(menor);

   /* bool ok;
    for (int i = 0; i < largo; i++) {
        ok = heap_encolar(heap, valores[i]);
        if (!ok) break;
    }
    if (debug) print_test("Prueba encolar muchos valores heap de maximo", ok);

    for (int i = largo-1; i >= 0; i--) {
        int* valor = heap_desencolar(heap);
        ok = ( *valor == i );
        if (!ok) break;
    }
    if (debug) print_test("Prueba desencolar muchos valores heap de maximo",1, ok);

    if (debug) print_test("Prueba heap esta vacio",1, heap_esta_vacio(heap));
    heap_destruir(heap, NULL); */

    /* Heap de minimo */ 
   /* heap = heap_crear(mayor);

    for (int i = 0; i < largo; i++) {
        ok = heap_encolar(heap, valores[i]);
        if (!ok) break;
    }
    if (debug) print_test("Prueba encolar muchos valores heap de minimo", ok);

    for (int i = 0; i < largo; i++) {
        int* valor = heap_desencolar(heap);
        ok = *valor == i;
        if (!ok) break;
    }
    if (debug) print_test("Prueba desencolar muchos valores heap de minimo",1, ok);

    if (debug) print_test("Prueba heap esta vacio",1, heap_esta_vacio(heap));
    heap_destruir(heap, NULL);

    for (int i = 0; i < largo; i++) {
        free(valores[i]);
    }
    free(valores);

}*/

    /* Genera y mezcla la lista de elementos */
int** generar_arreglo(size_t largo) {

    int **valores = malloc (largo * sizeof(int*));
    for (int i = 0; i < largo; i++) {
        valores[i] = malloc(sizeof(int));
        *valores[i] = i;
    }
    int temp;
    unsigned int seed = (unsigned int)time(NULL);
    srand (seed);
    for (size_t i = largo - 1; i > 0; i--) {
        size_t pos = rand() % i;
        temp = *valores[i]; 
        *valores[i] = *valores[pos];   
        *valores[pos] = temp;


    }
    return valores;
}

void prueba_heapsort(size_t largo) {
    int **valores = generar_arreglo(largo);
    heapsort((void**)valores, largo, comparame);
    bool ok;
    for (int i = 0; i < largo; i++) {
        ok = *(valores[i]) == i;
        if (!ok) break;
    }
    print_test("Prueba ordenar de menor a mayor",1, ok);

    heapsort((void**)valores, largo, mayor);
    for (int i = largo-1; i >= 0; i--) {
        ok = ( *(valores[(largo-1)-i]) == i );
        if (!ok) break;
    }
    print_test("Prueba ordenar de mayor a menor",1, ok);

    for (int i = 0; i < largo; i++) {
        free(valores[i]);
    }
    free(valores);
}

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/


int main(void) {
    prueba_heap();
    pruebas_volumen();
    prueba_ordenamiento();
    prueba_sort_volumen();
    prueba_encolar2_desencolar_encolar();
    prueba_heapsort(19);
    return 0;
}







