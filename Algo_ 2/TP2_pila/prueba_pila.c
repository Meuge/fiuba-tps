#include "pila.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/* ******************************************************************
 *                        PRUEBAS UNITARIAS
 * *****************************************************************/

/* Función auxiliar para imprimir si estuvo OK o no. */
void print_test(char* name, bool result, int valor)
{
    printf("%s: %s %d\n", name, result? "OK" : "ERROR", valor);
}

/* Prueba que las primitivas de la pila funcionen correctamente. */
void prueba_pila()
{

  int i;
  int j;

int num1;
int num2;
int num3;
int num4;
    /* Declaro las variables a utilizar */
 	pila_t * pila_1 = pila_crear(); /*Crea una pila*/

num1=131;
num2=261;
num3=33;
num4=938;

    print_test("Prueba que la pila desapile vacio",pila_desapilar(pila_1)==NULL,1);

    print_test("Prueba que la pila desapile vacio 2",pila_desapilar(pila_1)==NULL,1);

    print_test("Prueba que la pila vea tope vacio",pila_ver_tope(pila_1)==NULL,1);

  j=5;

 	print_test("Prueba pila_esta_vacia",pila_esta_vacia(pila_1),1); /*Prueba si la pila esta vacia.*/

  print_test("Prueba que la pila apile el numero 5",pila_apilar(pila_1,&j),5);

  j=10;

  print_test("Prueba que la pila apile el numero 10",pila_apilar(pila_1,&j),10);

  print_test("Prueba que no desapile un elemento no existente",*(int *)pila_desapilar(pila_1)!=78,1);





  	for (i=1;i<15;i++) /*Se apilan una cantidad de elementos*/
   {
       print_test("Prueba que la pila apile elementos",pila_apilar(pila_1, &i),i);
   }
    for(i=1;i<16;i++){ /*Desapila una cantidad de elementos*/
        print_test("Prueba que la pila desapile elementos",*(int *)pila_desapilar(pila_1)!=0,i);
}


    print_test("Prueba que la pila desapile elementos vacio",pila_desapilar(pila_1)==NULL,2);

    print_test("Desapila el elemento tope",pila_ver_tope(pila_1)==NULL,78); /*Prueba que el elemento coincida con el desapilado */

    print_test("Prueba que la pila apile elementos",pila_apilar(pila_1,&num1),num1);
    print_test("Prueba que la pila apile elementos",pila_apilar(pila_1,&num2),num2);
    print_test("Prueba que la pila apile elementos",pila_apilar(pila_1,&num3),num3);
    print_test("Prueba que la pila apile elementos",pila_apilar(pila_1,&num4),num4);

    print_test("Desapila el tope",*(int *)pila_desapilar(pila_1)==num4,num4); /*Prueba desapilar un elemento */

    print_test("Prueba el tope de la pila",*(int *)pila_ver_tope(pila_1)==num3,num3); /*Verifica el tope de la pila*/
    pila_destruir(pila_1);
    print_test("Prueba que la pila se destruya",true,1); /*destruye la pila*/
 }

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/


int main(void) {
    prueba_pila();
    return 0;
}



