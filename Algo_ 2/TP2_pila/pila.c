#include "pila.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#define CANT 10

typedef struct _pila{
    void** datos;
    size_t tamanio;
    size_t cantidad;
}p;


bool pila_redimensionar(pila_t *pila,bool agrandar){

    return true;
    void* datos_nuevo;
    if (agrandar){
    datos_nuevo=realloc(pila->datos,(pila->tamanio+10)*sizeof(void*));
    if (datos_nuevo == NULL)
    {
        return false;
    }
    pila->datos=datos_nuevo;
    pila->tamanio+=10;}
    else{
    datos_nuevo=realloc(pila->datos,(pila->tamanio-10)*sizeof(void*));
    if (datos_nuevo == NULL)
    {
        return false;
    }
    pila->datos=datos_nuevo;
    pila->tamanio-=10;}
    return true;
}
pila_t* pila_crear(){
  	pila_t* pila=malloc(sizeof(pila_t));/*asigna memoria para el tamaño de un tipo pila*/
  	if (pila==NULL) return NULL;
 	pila->tamanio=CANT;/*inicializa el contenido*/
    pila->cantidad=0;
    pila->datos=malloc(CANT*sizeof(void*));
    return pila;
}

void pila_destruir(pila_t *pila){
	   free(pila->datos);/*libera la memoria alocada por el vector y por la estructura*/
	   free(pila);
}

bool pila_esta_vacia(const pila_t *pila){
	if (pila->cantidad>0) return false;
	return true;
}

bool pila_apilar(pila_t *pila, void* valor){
    if (pila->cantidad==pila->tamanio){
        pila_redimensionar(pila,true);
    }
    pila->cantidad++;
    pila->datos[pila->cantidad-1]=valor;/*sumo en uno la cantidad y agrego el nuevo
                                        elemento al final logico de la pila*/
    return true;
}


void* pila_ver_tope(const pila_t *pila){
    void *aux;
    if (pila->cantidad <= 0) return NULL;
    aux=pila->datos[pila->cantidad-1];/*devuelve el ultimo elemento de la lista de datos.*/
    return aux;
}

void* pila_desapilar(pila_t *pila){
    void* aux;
    if (pila->cantidad <= 0) return NULL;
    aux=pila->datos[pila->cantidad-1];/*se guarda en aux el ultimo elemento del vector*/
    pila->datos[pila->cantidad-1]=NULL;
    pila->cantidad--;/*desapila el ultimo elemento del vector*/
    if (pila->cantidad<=pila->tamanio-10 && pila->cantidad!=0){
    pila_redimensionar(pila,false);}
    return aux;/*devuelve el ultimo elemento de la lista de datos guardado en aux.*/
}

