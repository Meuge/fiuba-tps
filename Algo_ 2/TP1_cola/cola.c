#include "cola.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>


typedef struct nodo
{
    struct nodo* siguiente;
    void* dato;
} t_nodo;

typedef struct cola
{
    t_nodo* primero;
    t_nodo* ultimo;
} t_cola;


t_nodo* nodo_crear()
{
    t_nodo* nodo = malloc(sizeof(t_nodo));
    if (nodo == NULL) return NULL;
    return nodo;
}

cola_t* cola_crear()
{
    cola_t* c_aux=malloc(sizeof(cola_t));
    if (c_aux == NULL) return NULL;
    c_aux->primero=NULL;
    c_aux->ultimo=NULL;
    return c_aux;
}

void cola_destruir(cola_t *cola, void destruir_dato(void*))
{
    t_nodo* aux=NULL;
    if (!cola_esta_vacia(cola))
    {
        while (cola->primero!=NULL)
        {
            aux=cola->primero->siguiente;
            if (destruir_dato!=NULL){
            destruir_dato(cola->primero->dato);}
            free(cola->primero);
            cola->primero=aux;
        }
    }
    free(cola);
}

bool cola_esta_vacia(const cola_t *cola)
{
    return (cola->primero==NULL);
}

void* cola_desencolar(cola_t *cola)
{
    t_nodo* nodo_aux;
    void* dato_aux;
    if (cola_esta_vacia(cola))
    {
        return NULL;
    }
    nodo_aux=cola->primero->siguiente;
    dato_aux=cola->primero->dato;
    free(cola->primero);
    cola->primero=nodo_aux;
    if (cola->primero==NULL)
    {
        cola->ultimo=NULL;
    }
    return dato_aux;
}

bool cola_encolar(cola_t *cola, void* valor)
{
    t_nodo *nodo;
    nodo=nodo_crear();
    if (nodo==NULL)return false;
    nodo->dato=valor;
    nodo->siguiente=NULL;
    if (cola->ultimo==NULL)
        cola->primero=nodo;
    else
        (cola->ultimo)->siguiente=nodo;
    cola->ultimo=nodo;
    return true;
}

void* cola_ver_primero(const cola_t *cola)
{
    void* prim;
    if (cola_esta_vacia(cola))
    {
        return NULL;
    }
    prim=(cola->primero)->dato;
    return prim;
}
