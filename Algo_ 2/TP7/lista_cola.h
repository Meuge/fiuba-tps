#ifndef LISTA_COLA_H
#define LISTA_COLA_H
#include "cola.h"
#include "lista.h"

/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

typedef struct lista_colas lista_cola_t;

/*********************************************************************
*                             FUNCIONES
* ********************************************************************/

datos_t* buscar_cliente(lista_cola_t* lista_col, const datos_t* data,bool borrar);

/*Agrega un nuevo elemento a la lista de pedidos. Devuelve falso en caso de error o de que el cliente ya exista.
 Pre: la lista fue creada.
 Post: se agregó un nuevo elemento a la lista, dato es el ultimo de la misma.*/
bool ingresar_pedido(const datos_t *valor, lista_cola_t* lista_col);


/*Modifica el pedido del cliente y devuelve verdadero
en caso de querer ser eliminado la cantidad de pizzas es de 0
Pre: la lista fue creada y contiene datos.
Post: Se modifica pedido en caso de existencia */
bool modificar_pedido(lista_cola_t* lista_col,datos_t *valor,int cantidad);


/* Devuelve verdadero e imprime los pedidos registrados
Pre:la lista fue creada */
bool imprimir_registros(lista_cola_t* lista_col);

/*Pide el ingreso de los datos que se piden al cliente cuando solicita una pizza
Pre:Se pide el ingreso de datos
Post: Se almacenan los datos*/
datos_t* ingresar_datos();

/* Muestra el menu de opciones que tiene el dueño de la pizzeria
Pre:Mostrar las opciones
Post:Llamar a las funciones que realizan cada accion*/
bool ver_menu();

/*Prepara el pedido ingresado
Pre:La lista fue creada
Post:Ingresa un nuevo elemento a la lista de pedidos preparados*/
bool preparar_pedido(lista_cola_t* lista_col);

/* Crea una lista de colas
Post: Crea una lista que tiene una cola de pedidos por zona, una lista de pedidos enviados, y una lista de pedidos siendo preparados*/
lista_cola_t* lista_cola_crear();

/*Imprime los elementos enviados por la lista 
Pre:La lista fue creada
Post:Imprime elementos enviados en caso de que haya existentes, devuelve booleano en caso de poseer elementos */
bool imprimir_enviados(lista_cola_t* lista_col);

/*Envia los pedidos 
Pre:La lista fue creda
Post:Envia pedidos en caso de que haya tres elementos en la cola */
bool enviar_pedido(lista_cola_t* lista_col);

/*Verifica que se envien todos los pedidos de la lista 
Pre:La lista fue creada
Post:No hay mas elementos para enviar*/
bool cerrar_caja(lista_cola_t* lista_col);

/*Destruye las estructuras utlizadas
Pre:La cola fue creada
Post:La cola es eliminada*/
void lista_cola_destruir(lista_cola_t* lista_col);

/*Imprime los elementos preparados por zona
Pre:La lista fue creada
Post:Devuelve un booleano en caso de que haya elementos para mostrar por pantalla */
bool imprimir_por_zona(lista_cola_t* lista_col);

#endif /* LISTA_COLA_H*/
