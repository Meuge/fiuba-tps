#ifndef CONSTANTES_H
#define CONSTANTES_H

#include <stdbool.h>
#include <stddef.h>

#define CANT_ZON 5
#define CANT_PIZ 5
#define CANT_ESP 3
#define BUFFERSIZE 20

typedef enum
{
    Z1=0,Z2=1,Z3=2,Z4=3,Z5=4
} zonas;

#endif /* CONSTANTES_H */
