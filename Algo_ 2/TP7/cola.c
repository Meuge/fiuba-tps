#include "cola.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>

typedef struct cola
{
    nodo_t* primero;
    nodo_t* ultimo;
} t_cola;


cola_t* cola_crear()
{
    cola_t* c_aux=malloc(sizeof(cola_t));
    if (c_aux == NULL) return NULL;
    c_aux->primero=NULL;
    c_aux->ultimo=NULL;
    return c_aux;
}

void cola_destruir(cola_t *cola, void destruir_dato(void*))
{
    nodo_t* aux=NULL;
    if (!cola_esta_vacia(cola))
    {
        while (cola->primero!=NULL)
        {
            aux=nodo_get_siguiente(cola->primero);
            if (destruir_dato!=NULL)
            {
                destruir_dato(nodo_get_dato(cola->primero));
            }
            free(cola->primero);
            cola->primero=aux;
        }
    }
    free(cola);
}

bool cola_esta_vacia(const cola_t *cola)
{
    return (cola->primero==NULL);
}

void* cola_desencolar(cola_t *cola)
{
    nodo_t* nodo_aux;
    void* dato_aux;
    if (cola_esta_vacia(cola))
    {
        return NULL;
    }
    nodo_aux=nodo_get_siguiente(cola->primero);
    dato_aux=nodo_get_dato(cola->primero);
    free(cola->primero);
    cola->primero=nodo_aux;
    if (cola->primero==NULL)
    {
        cola->ultimo=NULL;
    }
    return dato_aux;
}

bool cola_encolar(cola_t *cola, void* valor)
{
    nodo_t *nodo;
    nodo=nodo_crear(valor);
    if (nodo==NULL)return false;
    nodo_set_siguiente(nodo,NULL);
    if (cola->ultimo==NULL)
        cola->primero=nodo;
    else
        nodo_set_siguiente(cola->ultimo,nodo);
    cola->ultimo=nodo;
    return true;
}

void* cola_ver_primero(const cola_t *cola)
{
    void* prim;
    if (cola_esta_vacia(cola))
    {
        return NULL;
    }
    prim=nodo_get_dato(cola->primero);
    return prim;
}
