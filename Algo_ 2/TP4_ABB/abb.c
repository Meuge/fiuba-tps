#include "abb.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *copiar_clave(const char *clave)
{
    char *clave_aux=malloc(strlen(clave)+1);
    if (clave_aux==NULL) return NULL;
    strcpy(clave_aux, clave);
    return clave_aux;
}

nodo_abb_t* crear_nodo(void* dato,const char*clave,nodo_abb_t* padre)
{
    nodo_abb_t* nodo= malloc(sizeof(nodo_abb_t));
    if (nodo==NULL) return NULL;
    if(!(nodo->clave=copiar_clave(clave)))
    {
        free(nodo);
        return NULL;
    }
    nodo->dato = dato;
    nodo->izq = nodo->der = NULL;
    nodo->padre = padre;
    return nodo;
}
abb_t* abb_crear(abb_comparar_clave_t cmp, abb_destruir_dato_t destruir_dato)
{
    abb_t*arbol=malloc(sizeof(abb_t));
    if (arbol==NULL) return NULL;
    arbol->raiz=NULL;
    arbol->cantidad=0;
    arbol->cmp=cmp;
    arbol->destruir_dato=destruir_dato;
    return arbol;
}

size_t abb_cantidad(abb_t *arbol)
{
    if (arbol==NULL) return 0;
    return arbol->cantidad;
}

nodo_abb_t* buscar_dato(nodo_abb_t*nodo_raiz, const char *clave,abb_comparar_clave_t cmp)
{
    if (nodo_raiz==NULL) return NULL;
    if (cmp(nodo_raiz->clave, clave) == 0)
        return nodo_raiz;
    if (cmp(clave, nodo_raiz->clave) < 0)
        return buscar_dato(nodo_raiz->izq,clave,cmp);
    if (cmp(clave, nodo_raiz->clave) > 0)
        return buscar_dato(nodo_raiz->der,clave,cmp);
    return NULL;
}

bool abb_pertenece(const abb_t *arbol, const char *clave)
{
    nodo_abb_t* nodo=abb_obtener(arbol,clave);
    if (nodo!=NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void *abb_obtener(const abb_t *arbol, const char *clave)
{
    if (arbol==NULL) return NULL;
    nodo_abb_t*nodo=buscar_dato(arbol->raiz,clave,arbol->cmp);
    if (nodo==NULL) return NULL;
    return nodo->dato;
}

bool abb_guardar_auxiliar(nodo_abb_t**nodo_raiz, const char *clave,void*dato,abb_t* arbol,nodo_abb_t* nodo_padre)
{
    if (*nodo_raiz==NULL)
    {
        nodo_abb_t* nodo_nuevo=crear_nodo(dato,clave,nodo_padre);
        if(nodo_nuevo)
        {
            (*nodo_raiz)=nodo_nuevo;
            arbol->cantidad++;
            return true;
        }
        return false;
    }
	if(arbol->cmp(clave,(*nodo_raiz)->clave)==0){
        if (arbol->destruir_dato!=NULL){
		arbol->destruir_dato((*nodo_raiz)->dato);}
		(*nodo_raiz)->dato=dato;
		return true;}
    else if(arbol->cmp(clave, (*nodo_raiz)->clave) < 0)
        return abb_guardar_auxiliar(&(*nodo_raiz)->izq,clave,dato,arbol,*nodo_raiz);
    else if(arbol->cmp(clave, (*nodo_raiz)->clave) > 0)
        return abb_guardar_auxiliar(&(*nodo_raiz)->der,clave,dato,arbol,*nodo_raiz);
    return true;
}

bool abb_guardar(abb_t *arbol, const char *clave, void *dato)
{
    if (arbol==NULL) return false;
    if (abb_guardar_auxiliar(&arbol->raiz,clave,dato,arbol,NULL)) return true;
    return false;

}

void* abb_borrar_auxiliar(nodo_abb_t**nodo_raiz,const char* clave,abb_t *arbol)
{
    if (*nodo_raiz!=NULL)
    {
        if(arbol->cmp(clave,(*nodo_raiz)->clave)>0)
        {
            return abb_borrar_auxiliar(&(*nodo_raiz)->der,clave,arbol);
        }
        else if(arbol->cmp(clave,(*nodo_raiz)->clave)<0)
        {
            return abb_borrar_auxiliar(&(*nodo_raiz)->izq,clave,arbol);
        }
        else
        {
            nodo_abb_t* nodo_suprimir =(*nodo_raiz);
            if(nodo_suprimir->izq==NULL){
                (*nodo_raiz)=nodo_suprimir->der;
                if (nodo_suprimir->der!=NULL){
                nodo_suprimir->der->padre=nodo_suprimir->padre;}}
            else if (nodo_suprimir->der==NULL){
                (*nodo_raiz)=nodo_suprimir->izq;
                nodo_suprimir->izq->padre=nodo_suprimir->padre;}
            else
            {
                nodo_abb_t*nodo_temporal;
                nodo_abb_t*nodo_aux=nodo_suprimir;
                nodo_temporal=nodo_suprimir->izq;
                while(nodo_temporal->der)
                {
                    nodo_aux=nodo_temporal;
                    nodo_temporal=nodo_temporal->der;
                }
                nodo_suprimir->dato=nodo_temporal->dato;
                strcpy(nodo_suprimir->clave,nodo_temporal->clave);
                if(nodo_aux==nodo_suprimir){
                    nodo_aux->izq=nodo_temporal->izq;
                    if(nodo_aux->izq){
                    nodo_aux->izq->padre=nodo_aux;}}
                else{
                    nodo_aux->der=nodo_temporal->izq;
                    if(nodo_aux->der){
                    nodo_aux->der->padre=nodo_aux;}}
                    nodo_suprimir=nodo_temporal;
            }
            void* dato=nodo_suprimir->dato;
            free(nodo_suprimir->clave);
            free(nodo_suprimir);
            arbol->cantidad--;
            return dato;
        }
    }
    return NULL;
}

void *abb_borrar(abb_t *arbol, const char *clave)
{
    if (arbol==NULL) return false;
    if (!abb_pertenece(arbol,clave)) return false;
    return abb_borrar_auxiliar(&arbol->raiz,clave,arbol);
}

void abb_destruir_auxiliar(nodo_abb_t* arbol_nodo,abb_destruir_dato_t destruir_dato )
{
    if (arbol_nodo!=NULL)
    {
        abb_destruir_auxiliar(arbol_nodo->izq,destruir_dato);
        abb_destruir_auxiliar(arbol_nodo->der,destruir_dato);  //POSTORDEN
        if (destruir_dato!=NULL){
        destruir_dato(arbol_nodo->dato);}
        free(arbol_nodo->clave);
        free(arbol_nodo);
    }
}

void abb_destruir(abb_t *arbol)
{
    if(arbol!=NULL)
    {
        abb_destruir_auxiliar(arbol->raiz, arbol->destruir_dato);
        free(arbol);
    }
}

abb_iter_t *abb_iter_in_crear(const abb_t *arbol){
    abb_iter_t* iter=malloc(sizeof(abb_iter_t));
    if (iter!=NULL){
        iter->arbol=arbol;
        if (arbol!=NULL){
        iter->actual=arbol->raiz;
        while (arbol->raiz!=NULL && iter->actual->izq!=NULL){
            iter->actual=iter->actual->izq;
        }}else{
            iter->actual=NULL;
        }
        return iter;
    }else{
    return NULL;}
}

bool abb_iter_in_avanzar(abb_iter_t *iter){
    if (iter->actual!=NULL){
    if (iter->actual->der!=NULL){
        iter->actual=iter->actual->der;
        while (iter->actual->izq!=NULL){
            iter->actual=iter->actual->izq;
        }
        return true;
    }else{
        if (iter->actual->padre==NULL){
            iter->actual=NULL;
            return true;
        }
        while (iter->actual->padre!=NULL && iter->actual->padre->izq!=iter->actual){
            iter->actual=iter->actual->padre;
        }
        if (iter->actual->padre!=NULL){
            iter->actual=iter->actual->padre;
            return true;
        }else{
            iter->actual=NULL;
            return true;
        }
    }}else{
    return false;}
}

const char *abb_iter_in_ver_actual(const abb_iter_t *iter){
    if (iter->actual!=NULL){
    return iter->actual->clave;
    }else{
    return NULL;}
}

bool abb_iter_in_al_final(const abb_iter_t *iter){
    if (iter->actual==NULL){
        return true;
    }else{
        return false;
    }
}

void abb_iter_in_destruir(abb_iter_t* iter){
    free(iter);
}

void inorder(nodo_abb_t* arbol_nodo,bool funcion(const char *, void *, void *),void* extra){
  if (arbol_nodo != NULL) {
    inorder(arbol_nodo->izq,funcion,extra);
    funcion(arbol_nodo->clave,arbol_nodo->dato,extra);
    inorder(arbol_nodo->der,funcion,extra);
    }
}

void abb_in_order(abb_t *arbol, bool funcion(const char *, void *, void *), void *extra){
    if (arbol!=NULL){
        inorder(arbol->raiz,funcion,extra);
    }
}

