class Kml:

	def __init__(self,nombre):
		"""Crea el objeto kml"""
		self.ruta=""
		self.documento=[]
		self.nombre_archivo=nombre
		try:
			self.archivo=open(self.nombre_archivo,"w")
		except IOError:
			print"No es posible crear archivo"

	def encabezado(self):
		"""Crea el encabezado del kml"""
		self.documento.append('<?xml version="1.0" encoding="UTF-8"?>\n')
		self.documento.append('<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\n')
		self.documento.append("<Document>\n")
		self.documento.append("""<!-- Titulo del mapa -->\n""")
		self.documento.append('\t<name>Ruta de pedidos</name>\n')
		self.documento.append("""<!-- Final Titulo -->\n""")

	def agregar_marcadores(self,nombre,latitud,longitud):

		"""Agrega un marcador al kml"""
		self.documento.append("\t<Placemark>\n")
		self.documento.append("\t\t<name>" + nombre +" </name>\n")
		self.documento.append("\t\t<Point>\n")
		self.documento.append("\t\t\t <coordinates>"+latitud+","+longitud+"</coordinates>\n")
		self.documento.append("\t\t</Point>\n")
		self.documento.append("\t</Placemark>\n")

	def pie_documento(self):
		"""Realiza el pie del archivo kml"""
		self.documento.append("</Document>\n")
		self.documento.append("</kml>\n")
		self.archivo.writelines(self.documento)
		self.archivo.close()


	def linea(self,ruta,nombre):

				
    				self.documento.append("\t<Placemark>\n")
    				self.documento.append("\t\t<name>"+str(nombre)+"</name>\n")
    				self.documento.append("\t\t<LineString>\n")
    				self.documento.append("\t\t\t<coordinates>\n")	
    				self.documento.append("\t\t\t\t"+ruta+"\n")
    				self.documento.append("\t\t\t</coordinates>\n")
    				self.documento.append("\t\t</LineString>\n")
    				self.documento.append("\t</Placemark>\n")




