class Vertice:
    def __init__(self,clave):
        """Crea el vertice"""
        self.clave =clave
        self.adyacentes = {} #Crea un diccionario de vertices adyacentes

    def agregar_vecino(self,vecino,peso):
        """Agrega vertices adyacentes propiamente al vertice"""
        self.adyacentes[vecino] = peso #Agrega el peso del vecino objeto

    def __str__(self):
        """Imprime los adyacentes del vertice"""
        return str(self.clave) 

    def conseguir_conexiones(self):
        """Retorna los vertices adyacentes"""
        return self.adyacentes.keys()

    def conseguir_clave(self):
        """Retorna un vertice que posee adyacentes"""
        return self.clave

    def conseguir_peso(self,vecino):
        """Retona el peso de la arista"""
        return self.adyacentes[vecino]

class Grafo:
    def __init__(self):
        """Crea el grafo"""
        self.vertices = {} # clave es el nombre del nodo, valores es la clase vertices

    def agregar_vertice(self,clave):    
        """Agrega un vertice a la clase grafo"""
        nuevo_vertice= Vertice(clave) #crea un nuevo vertices
        self.vertices[clave] = nuevo_vertice
        return nuevo_vertice

    def obtener_vertice(self,vertice):
        """Retorna el objeto clase vertice"""
        if vertice in self.vertices:
            return self.vertices[vertice] #retorna objeto
        else:
            return None

    def __contains__(self,n):
        """Retorna el vertice si se encuentra en el grafo"""
        return n in self.vertices

    def obtener_vertice_objetos(self):
        """Retorna los objetos vertices"""
        return self.vertices.values()

    def agregar_arista(self,vertice,vecino,peso):
        """Agrega un vertice adyacente al actual con su peso de arista"""
        if vertice not in self.vertices:
            nv = self.agregar_vertice(vertice) #crea  vertice
        if vecino not in self.vertices:
            nv = self.agregar_vertice(vecino) #crea vecino
        self.vertices[vertice].agregar_vecino(self.vertices[vecino], peso)

    def obtener_vertices(self):
        """Retorna los vertices que componen el grafo"""
        return self.vertices.keys()

    def __iter__(self):
        """ Crea un iterador en grafo"""
        return iter(self.vertices.values())

