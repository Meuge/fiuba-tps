import csv
import os

def arhivo_texto():
	"""Recibe el archivo por parte del usuario valida que no este vacio y que el mismo exista"""
	encontrado=False
	while encontrado==False:
		try:
			arch=raw_input("Ingrese nombre del archivo:")
			archivo=open(arch)
			if os.path.getsize(archivo) > 0:
					encontrado=True
					archivo.close()
					return arch
			else:
				print "El archivo esta vacio"  
				encontrado=False
		except IOError:
			print "Nombre no valido"
		except OSError:
			print "Nombre no valido"
			
def menu():
	"""Muestra menu de las opciones que tiene el usuario con respecto al archivo de peliculas, valida que lo que ingrese este dentro del rango de lo pedido, llama a otras funciones de acuerdo a la eleccion"""
	while True:
		print "1: Si desea la ficha tecnica de la pelicula a ver"
		print "2: Si desea ver los actores con los que ha trabajado, dicho actor a ingresar"
		print "3: Si desea ver los directores con los que ha trabajado, dicho actor a ingresar"
		print "4: Si desea ver los generos en los que un actor o actriz ha desplegado su talento"
		print "5: Si desea ver la filmografia ordenada cronologicamente de un actor/actriz"
		print "6: Dado un actor o actriz y un director, mostrar las peliculas en donde trabajaron juntos"
		print "7: Dado un actor/actriz y un anio, mostrar las peliculas que realizo en el mismo"
		print "8: Dado un actor/actriz y otro actor/actriz mostrar las peliculas en que han trabajado juntos" 
		print "9: Dado un actor/actriz mostrar las peliculas en las que tambien cumple el rol de director"
		print "10: Si desea salir del programa\n\n\n"
		eleccion=raw_input("Ingrese el numero de la alternativa que desea ver:")
		opciones=["1","2","3","4","5","6","7","8","9","10"]
		if not(eleccion.isdigit()) or not(eleccion in opciones):
			print "CUIDADO, debe ingresar un valor numerico y dentro del rango de las alternativas, se le volvera a mostrar el menu""\n","\n","\n","\n"
		else:
			return eleccion
			break
			

def leer_archivotexto(archivo1):
	"""Se lee el archivo linea por linea, y se van conformando las distintas listas de los diferentes campos del archivo"""
	lista_peliculas=[],lista_directores=[],lista_actores=[]
	lista_genero=[],lista_anios=[]
	archivo_csv=csv.reader(open(archivo1,"rb"),delimiter=";")
	for pelicula,anio,director,actores,genero in archivo_csv:
		lista_peliculas.append(pelicula)
		lista_anios.append(anio)
		lista_directores.append(director)
		lista_genero.append(genero)
		lista_actores.append(actores)
	return lista_peliculas,lista_anios,lista_directores,lista_genero,lista_actores

			
def standarizar_formato(lista_peliculas,lista_directores,lista_genero,lista_actores):
	"""Se convierten todas las listas de los diferentes campos a cadena para convertilas a un mismo formato, se devuelven en forma de lista"""
	cadena_movies=",".join(lista_peliculas)
	cadena_titulo=cadena_movies.title() #Como no se de antemano en que formato puede estar el arhivo es decir algunos nombres en minusculas o mayusculas directamente se convierten todos al mismo formato
	lista_movies1=cadena_titulo.split(",")
	cadena_actores=";".join(lista_actores)
	cadena_actores2=cadena_actores.title()
	lista_actors1=cadena_actores2.split(";")
	return cadena_titulo,lista_movies1,lista_actors1,cadena_actores2


def ficha_tecnica(lista_peliculas,lista_anios,lista_directores,lista_genero,lista_actores):
	""" Imprime la ficha tecnica de la pelicula ingresada por el usuario, ademas valida que este en el archivo"""
	peli=raw_input("Ingrese la pelicula que desea conocer dicha ficha tecnica:")
	peli=peli.title()
	encontrado=False
	longitud=len(lista_peliculas)
	for i in range (longitud):
		if lista_peliculas[i]==peli:
			encontrado=True
			print "Pelicula:",lista_peliculas[i]
			print "Director:",lista_directores[i]
			print "Reparto:", lista_actores[i]
			print "Genero:", lista_genero[i]
			print "Anio:", lista_anios[i],"\n\n\n"
			break
	
	if encontrado==False:
		print peli, "El nombre de la pelicula ingresado no se encuentra en el archivo\n\n\n"
			

def ingresar_actor():
	"""Pide al usuario que ingrese un actor"""
	while True:
			actor=raw_input("Ingrese nombre del actor/actriz:")
			sin_espacios=actor.replace(" ","")
			if sin_espacios.isalpha()==True:
			
				actor=actor.title()
				return actor
			else:
				print "Ingrese caracteres"

		
def esta_en_la_lista(lista_nombres,nombre):
	"""Guarda en una lista los posibles resultados que coincidan con el nombre ingresado por el usuario, permite que el usuario ingrese o no el nombre completo"""
	cadena=(",").join(lista_nombres)
	lista=cadena.split(",")
	posibles_nombres=[]
	for i in lista:
		if nombre in i and i not in posibles_nombres: #como el usario puede ingresar el nombre completo o no, se crea una lista con los posibles resultados
			posibles_nombres.append(i)
	return posibles_nombres
		

def similar_nombres(lista_nombres,posibles_nombres,nombre):
	""" Valida que el nombre ingresado se encuentre en la lista de nombres"""
	longitud=(len(posibles_nombres))
	if longitud==0: #si la lista es vacia retorna que el actor no se encuentra en el archivo
		return False,nombre
	if longitud==1:
		actor=posibles_nombres[0]
		return True,actor #el nombre ingresado es correcto
	if longitud>0:
		print"Tal vez usted quiso referirse a algunos de los siguientes nombres"
		for i in xrange (len(posibles_nombres)): #permite que el usuario escoja que nombre realmente quiso escribir
			print i,":",posibles_nombres[i]
	eleccion=raw_input("Ingrese a cual nombre quiso referirse:")
	eleccion=int(eleccion)
	nombre=posibles_nombres[eleccion]
	return True, nombre

	
def actores_index(lista_actores,actor):
	"""Busca en que posicion se encuentra el actor en la lista y devuelve en una lista las posiciones"""
	posicion=[]
	for i in xrange (len(lista_actores)):
		if actor in lista_actores[i]:
			posicion.append(i)
	return posicion

		
def actores(lista_posicion,actor,lista_actores):
	""" Realiza una nueva lista con los actores que trabajaron con el actor ingresado por el usuario"""
	actores=[]
	for i in lista_posicion:
		if lista_actores[i] not in actores:#puesto que el archivo puede contener peliculas repetidas, se valida que en la nueva lista no haya actores repetidos ademas tambien pueden coincidir actores en diferentes peliculas
			actores.append(lista_actores[i])
			actores1=",".join(actores)
			actores1=actores1.replace(actor,"")
	print "\n\n\n",actor,"ha trabajado con los siguientes pares:",actores1,"\n\n\n"

	
def mostrar_directores(actor,lista_posicion,lista_directores):
	""" Busca los directores que trabajaron con el actor ingresado por el usuario"""
	directores=[]
	for i in lista_posicion:
		if lista_directores[i] not in directores: #Valida que el director no se encuentre mas de dos veces en la lista
			directores.append(lista_directores[i])
			directores1=",".join(directores)
	print actor,"ha trabajado con los siguientes directores:",directores1,".\n\n\n"


def actores_que_trabajaron_juntos():
	""" Pide al usuario que ingrese dos actores"""
	while True:
		a1=raw_input("Ingrese el primer actor/actriz:")
		a2=raw_input("Ingrese el segundo actor/actriz:")
		sin_espacios=a1.replace(" ","")
		sin-espacio=a2.replace(" ","")
		if sin_espacios.isalpha()==True and sin-espacio.isalpha()==True:
			a1=a1.title()
			a2=a2.title()
			return a1,a2
		else:
			print "Debe ingresar caracteres"


def actores_juntos(lista_actores,a1,a2):
	"""Busca si dos actores ingresado por el usuario han actuado juntos"""
	peliculas=[]
	a3=a1+","+a2 #Suma los dos actores en un mismo string para realizar la busqueda
	for i in xrange (len(lista_actores)):
		if a3 in lista_actores[i]:
			peliculas.append(i)		
	return peliculas

		
def actores_juntos1(peliculas,lista_peliculas,a1,a2):
	""" Muestra las peliculas en la que los dos actores ingresados por el usuario hayan trabajado juntos, en caso negativo informa al usuario"""
	posiciones=[]
	if (len(peliculas))==0:
		print a1,a2, "No han trabajado juntos\n\n\n"
	else:
		for i in peliculas:
			if lista_peliculas[i] not in posiciones:
				posiciones.append(lista_peliculas[i])
				peliculas1=",".join(posiciones)
		print "Las peliculas en las que han trabajado juntos",a1,a2,"son las siguientes:",peliculas1,"\n\n\n"


def actor_genero(actor,lista_posicion,lista_generos):
	"""Recibe el nombre de un actor/actriz, su posicion y una lista de generos. Busca los generos en las que trabajo dicho actor o actriz y los devuelve."""
	generos = []
	for i in lista_posicion:
		if (lista_generos[i] not in generos): #Verifica que el genero no se encuentre en la lista de generos y lo agrega.
			generos.append(lista_generos[i])
	generos1 = ", ".join(generos)
	print actor,"ha desplegado su talento en los siguientes generos:",generos1,"\n\n\n"


def actor_filmografia(actor,lista_posicion,lista_movies,lista_anios):
	"""Recibe el nombre de un actor/actriz, su posicion, una lista de peliculas y otra de anios. Busca las peliculas en las que trabajo dicho actor o actriz y devuelve su filmografia ordenada cronologicamente en un diccionario."""
	filmografia = []
	filmografia1 = []
	for i in lista_posicion:
		if (lista_anios[i],lista_movies[i]) not in filmografia: #Verifica que el par anio-pelicula no este en la lista, y lo agrega.
			filmografia.append((lista_anios[i],lista_movies[i]))
	filmografia.sort()
	for tupla in filmografia:
		filmografia1.append(str(tupla[0]+": "+tupla[1]))
	filmografia2 = ", ".join(filmografia1)
	print actor,"ha trabajado en las siguientes peliculas:",filmografia2,"\n\n\n"


def ingresar_director():
	"""Pide al usuario que ingrese un director"""
	while True:
                        director=raw_input("Ingrese nombre del director:")
                        sin_espacios=director.replace(" ","")
                        if sin_espacios.isalpha()==True:
		director=director.title()
		return director
                        else:
		print "Debe ingresar solo caracteres"


def actor_director_pelicula(actor,director,lista_posicion,lista_directores,lista_movies):
	"""Recibe el nombre de un actor/actriz, un director, una lista de posiciones del actor, una lista de directores y una lista de peliculas. Busca las peliculas en las que el actor y director recibidos trabajaron juntos y las imprime."""
	filmografia = []
	for i in lista_posicion:
		if (director == lista_directores[i]) and not(lista_movies[i] in filmografia): #Verifica que el director haya trabajado con el actor, que la pelicula no este en la lista y la agrega.
			filmografia.append(lista_movies[i])
	filmografia1 = ", ".join(filmografia)
	if (len(filmografia1) != 0):
		print actor,"y",director,"han trabajado en las siguientes peliculas:",filmografia1,"\n\n\n"
	else:
		print actor,"y",director,"no han trabajado juntos en ninguna pelicula.\n\n\n"


def ingresar_anio():
	"""Pide al usuario que ingrese un anio"""
	while True:
		anio=(raw_input("Ingrese un anio:"))
		if anio.isdigit():
			anio=str(anio)
			break
	return anio


def actor_anio_pelicula(actor,anio,lista_posiciones,lista_anios,lista_peliculas):
	"""Recibe el nombre de un actor, un anio, una lista de posiciones, una lista de anios y una lista de peliculas. Busca las peliculas en las que trabajo el actor en ese anio y las imprime."""
	filmografia = []
	for i in lista_posiciones:
		if (anio == lista_anios[i]) and (not(lista_peliculas[i] in filmografia)):
			filmografia.append(lista_peliculas[i])
	filmografia1 = ", ".join(filmografia)
	if (len(filmografia1) != 0):
		print actor,"en",anio,"ha protagonizado las siguientes peliculas:",filmografia1,"\n\n\n"
	else:
		print actor,"en",anio,"no ha protagonizado ninguna pelicula.\n\n\n"


def ambos_roles(actor,lista_posiciones,lista_directores,lista_peliculas):
	"""Busca las peliculas en las que un actor cumple ambos roles, actor y director."""
	filmografia=[]
	for i in lista_posiciones:
		if (actor in lista_directores[i]) and not(lista_peliculas[i] in filmografia):
			filmografia.append(lista_peliculas[i])
	filmografia1=", ".join(filmografia)
	if (len(filmografia) != 0):	
		print actor,"ha cumplido ambos roles, actor y director en las siguientes peliculas:",filmografia1,"\n\n\n"
	else:
		print actor,"no ha cumplido los roles de actor/director en ninguna pelicula."


def opciones(eleccion):
	""" Ejecuta las funciones en concordancia a las altenativas mostradas en el menu"""
	if eleccion=="1":
		ficha_tecnica(lista_movies2,lista_anios1,lista_directores1,lista_genero1,lista_actores1)
	if eleccion=="2":
		actor0=ingresar_actor()
		posibles_actores1=esta_en_la_lista(lista_actors2,actor0)
		valor,actor2=similar_nombres(lista_actors2,posibles_actores1,actor0)
		if valor==True:
			posicion=actores_index(lista_actors2,actor2)
			actores(posicion,actor2,lista_actors2)
		elif valor==False:
			print actor2, "no se encuentra en la base de datos, lo siento\n\n\n"
	if eleccion=="3":
		actor0=ingresar_actor()
		posibles_actores1=esta_en_la_lista(lista_actors2,actor0)
		valor,actor2=similar_nombres(lista_actors2,posibles_actores1,actor0)
		if valor==True:
			posicion=actores_index(lista_actors2,actor2)	
			mostrar_directores(actor2,posicion,lista_directores1)
		elif valor==False:
			print actor2, "no se encuentra en la base de datos, lo siento\n\n\n"
	if eleccion=="4":
		actor0=ingresar_actor()
		posibles_actores1=esta_en_la_lista(lista_actors2,actor0)
		valor,actor2=similar_nombres(lista_actors2,posibles_actores1,actor0)
		if valor==True:
			posicion=actores_index(lista_actors2,actor2)
			actor_genero(actor2,posicion,lista_genero1)
		elif valor==False:
			print actor2,"no se encuentra en la base de datos, lo siento\n\n\n"
	if eleccion=="5":
		actor0=ingresar_actor()
		posibles_actores1=esta_en_la_lista(lista_actors2,actor0)
		valor,actor2=similar_nombres(lista_actors2,posibles_actores1,actor0)
		if valor==True:
			posicion=actores_index(lista_actors2,actor2)
			actor_filmografia(actor2,posicion,lista_movies2,lista_anios1)
		elif valor==False:
			print actor2,"no se encuentra en la base de datos, lo siento\n\n\n"
	if eleccion=="6":
		actor=ingresar_actor()
		director=ingresar_director()
		posibles_actores=esta_en_la_lista(lista_actors2,actor)
		posibles_directores=esta_en_la_lista(lista_directores1,director)
		valor,actor1=similar_nombres(lista_actors2,posibles_actores,actor)
		valor1,director1=similar_nombres(lista_directores1,posibles_directores,director)
		if valor==True and valor1==True:
			posicion=actores_index(lista_actors2,actor1)
			actor_director_pelicula(actor1,director1,posicion,lista_directores1,lista_movies2)
		elif valor==False or valor1==False:
			print actor1,"o",director1,"no se encuentran en la base de datos, lo siento\n\n\n"
	if eleccion=="7":
		anio=ingresar_anio()
		actor0=ingresar_actor()
		posibles_actores1=esta_en_la_lista(lista_actors2,actor0)
		valor,actor2=similar_nombres(lista_actors2,posibles_actores1,actor0)
		if valor==True:
			posicion=actores_index(lista_actors2,actor2)
			actor_anio_pelicula(actor2,anio,posicion,lista_anios1,lista_movies2)
		elif valor==False:
			print actor2,"o",anio,"no se encuentra en la base de datos, lo siento\n\n\n"
	if eleccion=="8":
		actor4,actor5=actores_que_trabajaron_juntos()
		posibles_actores4=esta_en_la_lista(lista_actors2,actor4)
		posibles_actores5=esta_en_la_lista(lista_actors2,actor5)
		valor1,actor6=similar_nombres(lista_actors2,posibles_actores4,actor4)
		valor2,actor7=similar_nombres(lista_actors2,posibles_actores5,actor5)
		if valor1==True and valor2==True:
			pelis=actores_juntos(lista_actors2,actor6,actor7)
			actores_juntos1(pelis,lista_movies2,actor6,actor7)
		elif valor1==False or valor2==False:
			print actor6,"o", actor7,"no se encuentran en la base de datos, lo siento\n\n\n"
	if eleccion=="9":
		actor0=ingresar_actor()
		posibles_actores1=esta_en_la_lista(lista_actors2,actor0)
		valor,actor2=similar_nombres(lista_actors2,posibles_actores1,actor0)
		if valor==True:
			posicion=actores_index(lista_actors2,actor2)
			ambos_roles(actor2,posicion,lista_directores1,lista_movies2)
		elif valor==False:
			print actor2,"no ha sido actor y director en una misma pelicula, lo siento\n\n\n"



filmoteca=arhivo_texto()
lista_peliculas1,lista_anios1,lista_directores1,lista_genero1,lista_actores1=leer_archivotexto(filmoteca)
cadena_titulo2,lista_movies2,lista_actors2,cadena_actores2=standarizar_formato(lista_peliculas1,lista_directores1,lista_genero1,lista_actores1)
while True:
	eleccion=menu()
	if eleccion=="10":
		print "Nos vemos, pronto\n"
		filmoteca.close()
		break
	else:
		opciones(eleccion)

