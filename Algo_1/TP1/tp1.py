

import getpass,locale,random,string
from random import choice


def jugar():
	"""Inicializa el juego."""
	while True:	
		palabra=palabra_a_adivinar()
		adivinar(palabra)
		if desea_jugar() == False: #se llama a la funcion desea_jugar. Si devuelve True, vuelve a iniciar el juego, si devuelve False, lo termina.
			break


def palabra_a_adivinar():
	"""Pregunta al usuario la cantidad de jugadores. Si la respuesta es 1, devuelve una palabra al azar de una lista. Si es 2, llama a la funcion valida_palabra. Si no recibe alguno de estos 2 valores vuelve a preguntar."""
	while True:
		jugadores=raw_input("Ingrese la cantidad de jugadores (1 o 2): ")
		if (jugadores=="1"):
			palabras=['abigarrado','abyecto','adusto','atavico','baptisterio','brocado','conglomeracion','deferente','desidia','estuco','exorable','falacia','fehaciente','glutinoso','idiosincrasico']
			return random.choice(palabras)
		elif (jugadores=="2"):
			return valida_palabra()		


def valida_palabra():
	"""Valida que la palabra ingresada por el jugador sea una cadena y no tenga simbolos, sino vuelve a pedir la palabra a adivinar hasta que se ingrese una correcta. Luego de validar la palabra, la devuelve."""
	while True:
		palabra=getpass.getpass("Jugador 2, Ingrese la palabra a adivinar:")
		if (palabra.decode(codigo).isalpha()) == True:
			return palabra
		print "Por favor, ingrese una palabra valida.\n"


def ingresarletra():
	"""Valida que el usuario ingrese de a una letra y ademas que no sean simbolos o numeros."""
	while True:
		letra=raw_input("Jugador 1, Ingrese una letra: ")
		if (letra in string.lowercase and len(letra)==1):
			return letra
		else:
			print "Ingrese un solo caracter y que no sea simbolo o numero.\n"


def desea_jugar():
	"""Pregunta al usuario si desea a volver a jugar, esta funcion se presenta en caso del que usuario haya ganado o perdido los 6 intentos"""
	while True:
		respuesta=raw_input("\nIngrese (si), si desea volver a jugar, (no) en caso negativo: ")
		respuesta=respuesta.lower()
		if respuesta=="si":
			return True
		elif respuesta=="no":
			print "Espero que vuelvas a jugar pronto!\n"
			return False


def adivinar(palabra):
	"""Imprime la palabra parcialmente revelada, con las letras adivinadas y el simbolo "_" en el lugar de las letras sin adivinar. Muestra la cantidad de vidas, las letras ya utilizadas y las que quedan por usar."""
	intentos=6
	cadena=["_"]*len(palabra)
	letras_entradas=[]
	letras_restantes=list("abcdefghijklmnopqrstuvwxyz")
	num_letras_adivinadas=0
	
	while (intentos>0) and (num_letras_adivinadas!=len(palabra)): #verifica dos condiciones, que no se hayan acabado los intentos, y que no haya adivinado la palabra.
		letra=ingresarletra()
		letra=str(letra)
		if letra in letras_restantes:
			letras_restantes.remove(letra)
		if letra in letras_entradas: #verifica que no haya ingresado la letra dos veces, en caso afirmativo se le resta un intento.
			intentos-=1
			print "Ya ingresaste esa letra."
		else:
			letras_entradas.append(letra)
			letras_entradas.sort()
			if letra in palabra: #se fija si la letra se encuentra en la palabra a adivinar.
				num_letras_adivinadas+=palabra.count(letra)
				for i in range(len(palabra)): # en este ciclo se revisa cuantas veces la letra ingresada se encuentra en la palabra.
					if letra == palabra[i]:
						cadena[i] = letra
			else:
				intentos-=1
				print "Esa letra no se encuentra en la palabra."
		print "\nTe quedan la siguiente cantidad de vidas:",intentos
		print "Letras que te quedan por elegir:",letras_restantes
		print "Tus letras ingresadas son:",letras_entradas
		print "Esto es lo que has adivinado:",cadena,"\n"
		print cuerpo_del_ahorcado[intentos]

	if intentos==0: # en caso de que haya superado los intentos, se vuelve a preguntar si desea jugar.
		print "Perdiste, suerte en la proxima.\nLa palabra era",palabra
	if num_letras_adivinadas==len(palabra): #en caso de que haya ganado, se vuelve a preguntar si desea a jugar.
		print "Ganaste, Felicitaciones!\nLa palabra era",palabra


codigo=locale.getdefaultlocale()[1]
cuerpo_del_ahorcado=["Fuiste Ahorcado!\n"," O\n"," O\n |\n"," O\n/|\n"," O\n/|\\\n"," O\n/|\\\n/\n"," O\n/|\\\n/ \\\n"]
jugar()
