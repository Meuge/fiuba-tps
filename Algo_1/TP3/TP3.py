
import csv
import os
import simplejson
import random
from random import shuffle


class Nodo(object):
	
	def __init__ (self,dato=None,prox=None):
		self.dato = dato
		self.prox = prox
	def __str__ (self):
		return str(self.dato)
	
class ListaEnlazada(object):

	def __init__ (self):
		self.prim = None
		self.len = 0
	def __str__ (self):
		nodo = self.prim
		while nodo:
			print nodo
			nodo = nodo.prox
	def agregar(self,x):
		"""Agrega elementos a la lista enlazada"""
		self.len+=1
		nuevo = Nodo(x)
		if self.prim == None:
			self.prim = nuevo
		else:
			nodo = self.prim
			while nodo:
				anterior = nodo
				nodo = nodo.prox
			anterior.prox = nuevo

	def quitar(self,i=None):
		"""Quita un elemento de la lista enlazada"""
		if i<0 or i>0:
			raise IndexError("Se encuentra fuera de indice")
		if i==None:
			i=self.len-1
		if i==0:
			dato=self.prim.dato
			self.prim=self.prim.prox

		else:
			n_ant=self.prim
			n_act=n_ant.prox
			for pos in xrange(1,i):
				n_ant=n_act
				n_act=n_ant.prox

			dato=n_act.dato
			n_ant.prox=n_act.prox
			self.len-=1
			

class Jugador(object):

	def __init__(self,mazo):
		self.mazo = mazo
		self.Lista_enlazada = ListaEnlazada()
		self.lista_alternativa =[]
		self.partidas_ganadas = 0
		self.cartas_ganadas = []


	def traer_5_cartas(self):
		""" Extrae las primero 5 cartas del mazo y las agrega a la lista enlazada y a otra lista"""
		for i in range (5):
 			self.Lista_enlazada.agregar(self.mazo[i])
 			self.lista_alternativa.append(self.mazo[i])
 			self.mazo.pop(i)

 	
 	def quitar_cartas_que_sobren(self):
 		"""Extrae las cartas que sobraron de la mano"""
 		if len(self.lista_alternativa)!=0:
 			self.lista_alternativa.pop(0)
 			self.Lista_enlazada.quitar(0)
 		return self.lista_alternativa,self.Lista_enlazada

	def mostrar_cartas(self,dicc):
		"""Muestra las cartas del jugador, iterando en un diccionario"""
		for i in self.lista_alternativa:
			print i,dicc[i]


class Juego(object):
	
	def __init__(self):
		

		self.turno = 0
		self.partida = 0
		self.players = ['*','@']
		self.tablero_jugadores = [0]*9
		self.ganador = ""
		self.dicc={}
		self.tablero_cartas =  [0]*9
	
		self.matriz=([["", "",""],["", "", ""],["", "", ""],["", "", ""],["","",""],["","",""],["","",""],["","",""],["","",""]])


	
		self.cargar_cartas()

		lista_nombre2 = self.mezclar_cartas()

		mazo1,mazo2 = self.generar_mazos(lista_nombre2)

		self.generar_archivos_mazos(mazo1,mazo2)


		self.jugadores = [Jugador(mazo1),Jugador(mazo2)]

		self.iniciar_turno()


	def iniciar_turno(self):
		"""Realiza un random para ver quien comienza la partida"""
		self.turno = random.randrange(0,1)
  
		

	def cambiar_turno(self):
		"""Cambia turnos"""
		self.turno=1-self.turno


	def update_jugador(self,jugador,posicion):
		"""Actualiza la mano del jugador"""
		self.jugadores[posicion] = jugador


	def si_alguien_gano(self):
		"""Verifica si alguien gano"""

		quien_gano = 0

		if(self.jugadores[0].partidas_ganadas == 2):
			quien_gano = self.players[0]

		if(self.jugadores[1].partidas_ganadas == 2):
			quien_gano = self.players[1]


		if(quien_gano == 0):
			return False
		else:
			self.ganador = quien_gano
			return True
		


	def iniciar_juego(self):
		""" Inicia partida, siempre y cuando alguien no haya ganado"""

		while(self.si_alguien_gano() == False):

		
			self.matriz=([["", "",""],["", "", ""],["", "", ""],["", "", ""],["","",""],["","",""],["","",""],["","",""],["","",""]])
			self.tablero_cartas =  [0]*9
			self.tablero_jugadores = [0]*9
			self.jugadores[0].cartas_ganadas=[]
			self.jugadores[1].cartas_ganadas=[]
			print "Del grafico mostrado, tendra que elegir una posicion para alguna de las cartas","\n","\n","\n"
			print self.formato_board()
			self.iniciar_partida()
		
			
		print "El ganador es: ",str(self.ganador)


	
	def insertar_en_tablero(self,posicion,nombre_carta):
		""" Se inserta en la lista la posicion de la carta a ubicar en el tablero con su nombre y en la otra lista quien jugo esa carta"""
		x=int(posicion-1)
		self.tablero_cartas[x]=nombre_carta
		self.tablero_jugadores[x]=self.turno
		


	def iniciar_partida(self):
		""" Ejecuta la partida 9 veces, y trae las cartas pertenecientes a cada jugador, inicia turno"""
		cantidad_de_turnos=10
		self.iniciar_turno()
		self.jugadores[0].quitar_cartas_que_sobren()
		self.jugadores[1].quitar_cartas_que_sobren()
		self.jugadores[0].traer_5_cartas()
		self.jugadores[1].traer_5_cartas()

		
		print "EL jugador que comenzara la partida es Jugador: ",str(self.players[self.turno])
		
		for i in range(1,cantidad_de_turnos):
			self.ejecutar_turno()

		if self.buscar_ganador(0)> self.buscar_ganador(1):
			self.jugadores[0].partidas_ganadas+=1
		else:
			self.jugadores[1].partidas_ganadas+=1

	def buscar_ganador(self,i):
		 """ Busca que haya un ganador,cumpliendo el principio de que para ello debe de ganar dos partidas """
		 """ y para ganar cada partida debe de haber conquistado mas cartas en la misma"""
		 JugPos=[]
		 for z in xrange (9):
		 	if self.tablero_jugadores[z]==i:
		 		JugPos.append(z)
		 return len(self.jugadores[i].cartas_ganadas)+len(JugPos)-len(self.jugadores[1-i].cartas_ganadas)


	def chequear_posicion_disponibles(self,posicion,tablero_cartas):
		""" Se fija que la posicion a atacar contenga una carta en la lista"""
		x= int(posicion-1)
		if self.tablero_cartas[x]==0:
			return True
		else:
			return False

	def pedir_posicion(self):
		""" Se pide al usuario que ingrese la posicion en donde quiere ubicar la carta, se valida que esa posicion no este ocupada"""

		posicion = raw_input("Ingrese del 1 al 9 para referirse a cada casillero: ")
		posicion=int(posicion)

		while self.chequear_posicion_disponibles(posicion,self.tablero_cartas) == False:
			posicion = raw_input("Ingrese del 1 al 9 para referirse a cada casillero: ")
			posicion=int(posicion)

		return int(posicion)

			
	def ejecutar_turno(self):
		""" Pide al usuario la carta, la posicion, llama a otras funciones, y muestra pon pantalla si el usario ha ganado alguna carta o no"""
		print "Sus cartas son las siguientes","\n","\n","\n"
		jugador = self.jugadores[self.turno]
		
		print "Jugador",str(self.players[self.turno])

		jugador.mostrar_cartas(self.dicc)

		cantidad_de_cartas = len(jugador.lista_alternativa)

		text = "Ingrese del 1 al " , cantidad_de_cartas , " el la posicion de la carta: "
		carta = int(raw_input(text))
		
		nombre_carta = jugador.lista_alternativa[carta-1]
		jugador.lista_alternativa.pop(carta-1)
		jugador.Lista_enlazada.quitar(carta-1)
		
		posicion = self.pedir_posicion()
		self.insertar_en_tablero(posicion,nombre_carta)


		self.actualizar_tablero(nombre_carta,posicion)


		cartas_ganadas = self.atacar(posicion,nombre_carta)

		if len(cartas_ganadas) == 0:
			print "No has ganado ninguna carta!","\n","\n"
		else:
			for i in cartas_ganadas:
				if i in jugador.cartas_ganadas:
					break
				else:
					jugador.cartas_ganadas = jugador.cartas_ganadas + cartas_ganadas
		print "Sus cartas conquistadas son las siguientes ",str(jugador.cartas_ganadas),"\n","\n"


		self.update_jugador(jugador,self.turno)

		self.cambiar_turno()

	

	def orientacion_contraria(self,orientacion):
		"""Se define que punto cardinal ataca a otro"""
		dicc_contraria={"Sur":"Norte","Norte":"Sur","Este":"Oeste","Oeste":"Este"}
		orientacion_contra=dicc_contraria[orientacion]

		return orientacion_contra

	def traer_poder(self,nombre_carta,orientacion_carta):
		"""Trae los valores de las cartas a atacar"""

		return self.dicc[nombre_carta][orientacion_carta]

	def traer_mi_poder(self,nombre_carta,orientacion_carta_contraria):
		"""Se llama a funciones que retornar los valores de la carta que ataca y a los puntos cardinales que ataca"""
		
		orientacion_carta = self.orientacion_contraria(orientacion_carta_contraria)
		return self.traer_poder(nombre_carta,orientacion_carta)


	def atacar(self,mi_posicion,mi_carta):
		"""Ataca para ello llama a otras funciones que conforman el ataque"""
		posiciones,orientaciones = self.atacar_cartas(mi_posicion,mi_carta)
		posiciones_atacables,orientaciones_atacables = self.si_se_puede_atacar(posiciones,orientaciones)

		

		cartas_ganadas = []

		if len(posiciones_atacables) > 0 :

			for i in range(0,len(posiciones_atacables)):
				nombre_carta = self.tablero_cartas[posiciones_atacables[i]-1]
				orientacion_carta = orientaciones_atacables[i]

				poder_otro = self.traer_poder(nombre_carta,orientacion_carta)

				mi_poder = self.traer_mi_poder(mi_carta,orientacion_carta)

				if(poder_otro < mi_poder):
					cartas_ganadas.append(nombre_carta)

		
		return cartas_ganadas



	def si_se_puede_atacar(self,posicion,orientacion):
		""" Verifica que la carta a atacar no sea propia y que si hay alguien a quien atacar"""

		posiciones_atacables = []
		orientaciones_atacables = []

		for i in range(0,len(posicion)):
			if(self.tablero_cartas[posicion[i]-1] != 0 and self.tablero_jugadores[posicion[i]-1] != self.turno):
				posiciones_atacables.append(posicion[i])
				orientaciones_atacables.append(orientacion[i])

		return posiciones_atacables,orientaciones_atacables


	def atacar_cartas(self,mi_posicion,nombre_carta):
		"""Retorna a que punto cardinal puede atacar y la posiciones de las cartas a atacar"""
		
		if mi_posicion==1:
			lista = [2,4]
			orientacion = ['Oeste','Norte']
			
		if mi_posicion==2:
			lista=[1,3,5]			
			orientacion = ['Este','Oeste','Norte']

		if mi_posicion==3:
			lista=[2,6]
			orientacion = ['Este','Norte']

		if mi_posicion==4:
			lista=[1,5,7]
			orientacion = ['Sur','Oeste','Norte']

		if mi_posicion==5:
			lista=[2,4,6,8]
			orientacion = ['Sur','Este','Oeste','Norte']

		if mi_posicion==6:
			lista=[3,5,9]
			orientacion = ['Sur','Este','Norte']

		if mi_posicion==7:
			lista=[4,8]
			orientacion = ['Sur','Oeste']

		if mi_posicion==8:
			lista=[5,7,9]
			orientacion = ['Sur','Este','Oeste']

		if mi_posicion==9:
			lista=[6,8]
			orientacion = ['Sur','Este']

		return lista,orientacion
		

	def cargar_cartas(self):
		""" Se llama a funciones de leer el archivo y la que crea el diccionario"""
		while True:
			texto = self.arhivo_texto()
			self.lista_nombre,lista_suma= self.leer_archivotexto(texto)
			respuesta=self.preguntar_usuario()
			valor=self.promedio(lista_suma)
			if respuesta==True and valor==False:
				print "Se le solicitara que ingrese otro archivo para que cumpla con un promedio menor a 7"
				continue
			else:
				break



	def arhivo_texto(self):
		"""Recibe el archivo por parte del usuario valida que no este vacio y que el mismo exista"""
		encontrado=False
		while encontrado==False:
			try:
				arch=raw_input("Ingrese nombre del archivo:")
				arch=str(arch)
				size=os.path.getsize(arch)
				if size>0:
						encontrado=True
						return arch
				else:
					print "El archivo esta vacio"  
					encontrado=False
			except OSError:
					print "Nombre no valido"
			except EOFError:
					print "Nombre no valido"


	def leer_archivotexto(self,texto):
		""" Se lee el archivo, se crea un diccionario y dos listas"""
		lista_nombre=[]
		lista_suma=[]
		archivo =open(texto,"r")
		archivo_csv=csv.reader (archivo,delimiter=";")
		for row in archivo_csv:
			lista_nombre.append(row[0])
			self.dicc[row[0]] = {'Norte': int(row[1]), 'Sur': int(row[2]), 'Este': int(row[3]),'Oeste': int(row[4])}
			lista_suma.append(int(row[1])+int(row[2])+ int(row[3])+ int(row[4]))
		archivo.close()
		return lista_nombre,lista_suma

	def preguntar_usuario(self):
		""" Pregunta al usuario de que modo desea jugar"""
		eleccion=raw_input("Ingrese (s) si desea jugar con un mazo de promedio menor a 7 o (n) para no:")
		if eleccion=="s":
			return True
		elif eleccion=="n":
			return False

	def actualizar_tablero(self,nombre_carta,posicion):
				"""Se inserta la carta en el tablero"""
				Norte=str(self.dicc[nombre_carta]["Norte"])
				Sur=str(self.dicc[nombre_carta]["Sur"])
				Este=str(self.dicc[nombre_carta]["Este"])
				Oeste=str(self.dicc[nombre_carta]["Oeste"])
				if posicion==1:
					self.matriz[0][0]=Norte
					self.matriz[1][0]=Oeste+ " "+ Este
					self.matriz[2][0]=Sur
				if posicion==2:
					self.matriz[0][1]=Norte
					self.matriz[1][1]=Oeste+ " "+ Este
					self.matriz[2][1]=Sur
				if posicion==3:
					self.matriz[0][2]=Norte
					self.matriz[1][2]=Oeste+ " "+ Este
					self.matriz[2][2]=Sur
				if posicion==4:
					self.matriz[3][0]=Norte
					self.matriz[4][0]=Oeste+ " "+ Este
					self.matriz[5][0]=Sur
				if posicion==5:
					self.matriz[3][1]=Norte
					self.matriz[4][1]=Oeste+ " "+ Este
					self.matriz[5][1]=Sur
				if posicion==6:
					self.matriz[3][2]=Norte
					self.matriz[4][2]=Oeste+ " " +Este
					self.matriz[5][2]=Sur
				if posicion==7:
					self.matriz[6][0]=Norte
					self.matriz[7][0]=Oeste+ " "+ Este
					self.matriz[8][0]=Sur
				if posicion==8:
					self.matriz[6][1]=Norte
					self.matriz[7][1]=Oeste+ " "+ Este
					self.matriz[8][1]=Sur
				if posicion==9:
					self.matriz[6][2]=Norte
					self.matriz[7][2]=Oeste+ " "+ Este
					self.matriz[8][2]=Sur



				print self.formato_board()


	def promedio(self,lista_suma):
		"""Realiza un promedio del mazo"""
		longitud=len(self.lista_nombre)
		suma=0
		suma+=sum(lista_suma)
		promedio=suma/longitud
		if promedio>7:
			return False
		else:
			return True

	def mezclar_cartas(self):
		"""Se mezclan las cartas"""
		longitud=len(self.lista_nombre)
		for i in range(longitud+1):
			shuffle(self.lista_nombre)
		return self.lista_nombre

	def formato_columna(self,row):
    		return '|'+'__ __ __' .join('{0:^9s}'.format(x) for x in row) + '|'


	def formato_board(self):
    
    		return '\n\n'.join(self.formato_columna(row) for row in self.matriz)	

	def generar_mazos(self,lista_nombre):
		""" Se divide la lista de nombres de carta en dos, si no puede ser par un mazo tendra una carta mas que el otro"""
		longitud = len(lista_nombre)
		return [ lista_nombre[longitud::2] for longitud in xrange(2) ]

	def generar_archivos_mazos(self,mazo11,mazo22):
		""" Se realizan los dos archivos de los jugadores"""
		arch_mazo1=open("mazo1.1","w")
		arch_mazo2=open("mazo2.2","w")
		simplejson.dump(mazo11, arch_mazo1)
		simplejson.dump(mazo22, arch_mazo2)
		arch_mazo1.close()
		arch_mazo2.close()

		
	


juego = Juego()
juego.iniciar_juego()



